﻿using MyProject.Players;
using UnityEngine;

namespace MyProject.Controllers
{
    public class PlayerMover : MonoBehaviour
    {
        [SerializeField] private Player _player;

        private Transform _transform;

        private void Awake()
        {
            _transform = transform;
        }

        public void PlayerInput(PlayerInputMessage message)
        {
            var speed = _player.moveSpeed;
            var delta = new Vector3(speed * message.movementDirection.x, 0, speed * message.movementDirection.y) * message.deltaTime;
            _transform.position += delta;
            _transform.forward = new Vector3(message.aimDirection.x, 0, message.aimDirection.y);
        }
    }

    public class PlayerInputMessage
    {
        public Vector2 movementDirection { get; private set; }
        public Vector2 aimDirection { get; private set; }
        public float deltaTime { get; private set; }

        public PlayerInputMessage(Vector2 movementDirection, Vector2 aimDirection, float deltaTime)
        {
            this.movementDirection = movementDirection;
            this.aimDirection = aimDirection;
            this.deltaTime = deltaTime;
        }
    }
}
