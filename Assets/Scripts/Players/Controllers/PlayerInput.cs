﻿using MyProject.Players;
using UnityEngine;

namespace MyProject.Controllers
{
    public class PlayerInput : MonoBehaviour
    {
        [SerializeField] private Player _player;
        [SerializeField] private PlayerMover _playerMover;

        private Camera _camera;

        public void Initialize(Camera camera)
        {
            _camera = camera;
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Q))
            {
                _player.inventory.ChooseWeapon();
            }
        }

        private void FixedUpdate()
        {
            var moveInput = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));

            var ray = _camera.ScreenPointToRay(Input.mousePosition);

            var plane = new Plane(Vector3.up, Vector3.up * _player.publicTransform.position.y);
            plane.Raycast(ray, out var enter);
            var aimPos = ray.GetPoint(enter);
            var aimDistanceHelper = aimPos - _player.publicTransform.position;

            _player.SetAimDistance(aimDistanceHelper);
            _playerMover.PlayerInput(new PlayerInputMessage(
                moveInput.normalized,
                new Vector2(aimDistanceHelper.x, aimDistanceHelper.z).normalized,
                Time.fixedDeltaTime));
        }
    }
}