﻿using MyProject.Players;
using UnityEngine;

namespace MyProject.Controllers
{
    public class PlayerInputAttack : MonoBehaviour
    {
        private Player _player;

        public void Initialize(Player player)
        {
            _player = player;
        }

        private void Update()
        {
            var grenadeActivate = Input.GetKeyDown(KeyCode.Mouse1);

            if (grenadeActivate)
            {
                _player.inventory.grenade.Fire();
            }

            if (_player.inventory.currentWeapon.GetShootingType())
            {
                _player.inventory.currentWeapon.Fire();
            }

            var reload = Input.GetKeyDown(KeyCode.R);

            if (reload)
            {
                _player.inventory.currentWeapon.Reload();
            }
        }
    }
}