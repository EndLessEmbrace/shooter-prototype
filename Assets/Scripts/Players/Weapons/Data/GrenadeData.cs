﻿using UnityEngine;

namespace MyProject.Weapons.Data
{
    public class GrenadeData
    {
        public string id;
        public float maxDistance;
        public int damage;
        public int timeExplosion;
        public int attackDistance;
        public int angle;
        public int timeReload;
        public Sprite icon;
    }
}