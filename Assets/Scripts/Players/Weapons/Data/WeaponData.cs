﻿using UnityEngine;

namespace MyProject.Weapons.Data
{
    public class WeaponData
    {
        public string id;
        public float rateOfFire;
        public float timeReload;
        public int damage;
        public int attackDistance;
        public int maxCountAmmunition;
        public int numberBulletsCreated;
        public float spreadBullets;
        public Sprite icon;
    }
}
