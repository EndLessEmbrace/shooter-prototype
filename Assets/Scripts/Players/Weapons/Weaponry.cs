﻿using System.Collections.Generic;
using UnityEngine;

namespace MyProject.Weapons
{
    public class Weaponry : MonoBehaviour
    {
        [SerializeField] private Weapon _baseWeapon;
        [SerializeField] private List<Weapon> _allWeapons = new List<Weapon>();

        public Weapon baseWeapon => _baseWeapon;
        public List<Weapon> allWeapons => _allWeapons;
    }
}
