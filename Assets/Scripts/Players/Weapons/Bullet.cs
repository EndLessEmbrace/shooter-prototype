﻿using MyProject.Constants;
using UnityEngine;

namespace MyProject.Weapons
{
    public class Bullet : MonoBehaviour
    {
        [SerializeField] private float _speed;
        [SerializeField] private float _timer = 2;

        private Vector3 _targetTransform;
        private Transform _transform;

        private void Awake()
        {
            _transform = transform;

            Destroy(gameObject, _timer);
        }

        private void LateUpdate()
        {
            _transform.rotation = Camera.main.transform.rotation;
        }

        private void Update()
        {
            if (_transform.position == _targetTransform)
            {
                Destroy(gameObject);
            }

            _transform.position = Vector3.MoveTowards(_transform.position, _targetTransform, Time.deltaTime * _speed);
        }

        public void Initialize(Vector3 targetTransform)
        {
            _targetTransform.x = targetTransform.x;
            _targetTransform.y = MyProjectConstants.targetPositionY;
            _targetTransform.z = targetTransform.z;
        }
    }
}