﻿using System.Collections;
using System.Collections.Generic;
using MyProject.Units;
using MyProject.Weapons.Data;
using UnityEngine;

namespace MyProject.Weapons
{
    public class CommonGrenade : MonoBehaviour
    {
        private UnitsHolder _unitsHolder;
        private GrenadeData _data;
        private float _timeExplosion;

        private Coroutine _coroutine;
        private Transform _transform;

        private void Awake()
        {
            _transform = transform;
        }

        private void OnCollisionEnter(Collision collision)
        {
            if (_coroutine != null)
            {
                StopCoroutine(_coroutine);
            }

            StartCoroutine(Boom(0));
        }

        public void Initialize(UnitsHolder unitsHolder, GrenadeData data, float timeExplosion)
        {
            _unitsHolder = unitsHolder;
            _data = data;
            _timeExplosion = timeExplosion;

            _coroutine = StartCoroutine(Boom(_timeExplosion));
        }

        private IEnumerator Boom(float timeExplosion)
        {
            yield return new WaitForSeconds(timeExplosion);

            var allUnits = _unitsHolder.GetAllUnits();
            var damagedUnits = new List<Unit>();

            foreach (var unit in allUnits)
            {
                Vector3 offset = unit.publicTransform.position - _transform.position;
                float sqrMagnitude = offset.sqrMagnitude;

                if (sqrMagnitude < _data.attackDistance * _data.attackDistance)
                {
                    damagedUnits.Add(unit);
                }
            }

            foreach (var unit in damagedUnits)
            {
                Physics.Raycast(
                    transform.position,
                    (unit.publicTransform.position - _transform.position).normalized,
                    out RaycastHit hitInfo,
                    _data.attackDistance);

                if (hitInfo.transform != null)
                {
                    if (hitInfo.transform.gameObject.TryGetComponent(out Unit unitComponent))
                    {
                        unitComponent.TakeDamage(_data.damage);
                    }
                }
            }

            Destroy(gameObject);
        }
    }
}
