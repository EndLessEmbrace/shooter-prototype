﻿using System;
using System.Collections;
using MyProject.Other;
using MyProject.Units;
using MyProject.Weapons.Data;
using Newtonsoft.Json;
using UnityEngine;

namespace MyProject.Weapons
{
    public abstract class Weapon : MonoBehaviour
    {
        public event Action<DataShoot> Shoot = delegate { };
        public event Action StartReload = delegate { };
        public event Action EndingReload = delegate { };

        [SerializeField] private GameObject _model;
        [SerializeField] private TextAsset _weaponData;
        [SerializeField] private Bullet _bullet;
        [SerializeField] private Transform _projectileSpawner;

        [SerializeField] private ShootInputType _shootingType;

        private Unit _unit;

        protected Coroutine _reloadCoroutine;
        protected bool _reloading;

        #region parameters
        public string id { get; private set; }
        public float timeReload { get; private set; }
        public float rateOfFire { get; private set; }
        public int damage { get; private set; }
        public int numberBulletsCreated { get; private set; }
        public float spreadBullets { get; private set; }
        public int attackDistance { get; private set; }
        public int currentCountAmmunition { get; protected set; }
        public int maxCountAmmunition { get; private set; }
        #endregion

        public float lastTimeFire { get; protected set; }

        public Transform projectileSpawner => _projectileSpawner;
        public WeaponData weaponData { get; private set; }

        private void Awake()
        {
            Initialize();
        }

        public void Initialize()
        {
            var pathAutomaticData = _weaponData.text;
            weaponData = JsonConvert.DeserializeObject<WeaponData>(pathAutomaticData);

            id = weaponData.id;
            timeReload = weaponData.timeReload;
            rateOfFire = weaponData.rateOfFire;
            damage = weaponData.damage;
            numberBulletsCreated = weaponData.numberBulletsCreated;
            spreadBullets = weaponData.spreadBullets;
            attackDistance = weaponData.attackDistance;
            currentCountAmmunition = weaponData.maxCountAmmunition;
            maxCountAmmunition = weaponData.maxCountAmmunition;

            weaponData.icon = SpritesData.instance.GetSprite(id);
        }

        public void Initialize(Unit unit)
        {
            _unit = unit;
        }

        public bool GetShootingType()
        {
            return _shootingType.GetInput();
        }

        public virtual void Activate()
        {
            _model.SetActive(true);
        }

        public virtual void Deactivate()
        {
            _model.SetActive(false);

            if (_reloadCoroutine != null)
            {
                StopCoroutine(_reloadCoroutine);
                _reloading = false;
                EndingReload?.Invoke();
            }
        }

        public virtual void Fire()
        {
            if (_reloading || Time.time - rateOfFire < lastTimeFire)
                return;

            if (currentCountAmmunition < numberBulletsCreated)
            {
                Reload();
                return;
            }

            lastTimeFire = Time.time;

            var directions = SpreadDirection(_unit.publicTransform.forward, numberBulletsCreated, spreadBullets);

            foreach (var direction in directions)
            {
                Physics.Raycast(
                    _unit.publicTransform.position,
                    direction,
                    out RaycastHit hitInfo,
                   attackDistance);

                var bullet = Instantiate(_bullet, projectileSpawner.transform.position + direction, projectileSpawner.transform.rotation);

                if (hitInfo.transform != null)
                {
                    bullet.Initialize(hitInfo.point);
                }
                else
                {
                    var point = _unit.publicTransform.position + attackDistance * direction;
                    bullet.Initialize(point);
                }

                if (hitInfo.transform != null)
                {
                    if (hitInfo.transform.gameObject.TryGetComponent(out Unit unit))
                    {
                        if (!_unit.fraction.type.Equals(unit.fraction.type))
                        {
                            unit.TakeDamage(damage);
                        }
                    }
                }
            }

            currentCountAmmunition -= numberBulletsCreated;

            if (currentCountAmmunition <= 0)
            {
                Reload();
            }

            Shoot?.Invoke(new DataShoot(_unit.id, id, damage));
        }

        public virtual void Reload()
        {
            if (_reloading || currentCountAmmunition >= maxCountAmmunition)
                return;

            _reloadCoroutine = StartCoroutine(ReloadEnumerator());
        }

        private IEnumerator ReloadEnumerator()
        {
            StartReload?.Invoke();

            _reloading = true;
            yield return new WaitForSeconds(timeReload);

            currentCountAmmunition = maxCountAmmunition;
            _reloading = false;

            EndingReload?.Invoke();
        }

        private Vector3[] SpreadDirection(Vector3 startPoint, int countBullets, float spreadAngle)
        {
            Vector3[] result = new Vector3[countBullets];

            var halfCountBullets = Mathf.CeilToInt((float)countBullets / 2);

            result[0] = startPoint;

            var range = -spreadAngle;

            for (int i = 1; i < halfCountBullets; i++)
            {
                result[i] = startPoint + _unit.publicTransform.TransformDirection(new Vector3(range, 0, 0));
                range -= spreadAngle;
            }

            range = spreadAngle;

            for (int i = halfCountBullets; i < countBullets; i++)
            {
                result[i] = startPoint + _unit.publicTransform.TransformDirection(new Vector3(range, 0, 0));
                range += spreadAngle;
            }

            return result;
        }
    }

    public class DataShoot
    {
        public string idAttacker { get; private set; }
        public string idWeapon { get; private set; }
        public int damage { get; private set; }

        public DataShoot(string idAttacker, string idWeapon, int damage)
        {
            this.idAttacker = idAttacker;
            this.idWeapon = idWeapon;
            this.damage = damage;
        }
    }

    [Serializable]
    public class ShootInputType
    {
        [SerializeField] private ShootingType _type;

        public bool GetInput()
        {
            switch (_type)
            {
                case ShootingType.Automatic:
                    {
                        return Input.GetKey(KeyCode.Mouse0);
                    }
                case ShootingType.SemiAutomatic:
                    {
                        return Input.GetKeyDown(KeyCode.Mouse0);
                    }
                default: throw new Exception("This _type of weapon does not exist");
            }
        }
    }

    public enum ShootingType
    {
        Automatic = 0,
        SemiAutomatic = 1
    }
}