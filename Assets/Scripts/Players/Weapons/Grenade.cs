﻿using System;
using System.Collections;
using MyProject.Other;
using MyProject.Units;
using MyProject.Weapons.Data;
using Newtonsoft.Json;
using UnityEngine;

namespace MyProject.Weapons
{
    public class Grenade : MonoBehaviour
    {
        public event Action StartReload = delegate { };
        public event Action EndingReload = delegate { };

        [SerializeField] private Transform _spawnPoint;
        [SerializeField] private CommonGrenade _grenade;
        [SerializeField] private TextAsset _data;
        [SerializeField] private TrajectoryVisualization _trajectoryVisualization;

        private bool _reloading;
        private Unit _unit;

        private GrenadeData data;

        public void Initialize(Unit unit)
        {
            _unit = unit;

            var grenadeData = _data.text;
            data = JsonConvert.DeserializeObject<GrenadeData>(grenadeData);
        }

        public void Fire()
        {
            if (_reloading)
                return;

            StartCoroutine(Throw());
        }

        private IEnumerator Throw()
        {
            while (true)
            {
                var force = _unit.aimDistanceHelper + Vector3.up * data.angle;
                var forceClamp = Vector3.ClampMagnitude(force, data.maxDistance);

                _trajectoryVisualization.Show(_spawnPoint.transform.position, forceClamp);

                if (Input.GetKeyUp(KeyCode.Mouse1))
                {
                    _trajectoryVisualization.Activate(false);

                    var grenade = Instantiate(_grenade, _spawnPoint.transform.position, Quaternion.identity);
                    grenade.GetComponent<Rigidbody>().AddForce(forceClamp, ForceMode.VelocityChange);

                    grenade.Initialize(_unit.unitsHolder, data, data.timeExplosion);

                    Reload();
                    yield break;
                }

                yield return null;
            }
        }

        public virtual void Reload()
        {
            if (_reloading)
                return;

            StartCoroutine(ReloadEnumerator());
        }

        private IEnumerator ReloadEnumerator()
        {
            StartReload?.Invoke();

            _reloading = true;
            yield return new WaitForSeconds(data.timeReload);

            _reloading = false;

            EndingReload?.Invoke();
        }
    }
}
