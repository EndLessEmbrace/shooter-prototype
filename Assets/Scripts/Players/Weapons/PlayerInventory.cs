﻿using System;
using MyProject.Players;
using UnityEngine;

namespace MyProject.Weapons
{
    [RequireComponent(typeof(Player))]
    public class PlayerInventory : MonoBehaviour
    {
        public event Action ChangeWeapon = delegate { };

        [SerializeField] private Weaponry _weaponry;
        [SerializeField] private Grenade _grenade;

        private Player _player;

        public Weaponry weaponry => _weaponry;
        public Weapon currentWeapon { get; private set; }
        public Grenade grenade => _grenade;

        private void Awake()
        {
            _player = GetComponent<Player>();

            currentWeapon = _weaponry.baseWeapon;
            currentWeapon.Activate();
            currentWeapon.Initialize(_player);
            _grenade.Initialize(_player);
        }

        private void Start()
        {
            ChangeWeapon?.Invoke();
        }

        public void ChooseWeapon()
        {
            foreach (var weapon in _weaponry.allWeapons)
            {
                if (weapon != currentWeapon)
                {
                    currentWeapon.Deactivate();
                    currentWeapon = weapon;
                    currentWeapon.Activate();
                    currentWeapon.Initialize(_player);
                    ChangeWeapon?.Invoke();
                    return;
                }
            }
        }
    }
}