﻿using MyProject.Units;
using UnityEngine;

namespace MyProject.Players.Camera
{
    public class CameraFollowPlayer : MonoBehaviour
    {
        [SerializeField] private UnitsHolder _unitsHolder;
        [Range(0.5f, 1.0f)]
        [SerializeField] private float _smoothFactor = 0.5f;
        [SerializeField] private Vector3 _cameraOffset;

        private Transform _player;
        private Transform _transform;

        private void Awake()
        {
            _transform = transform;
        }

        private void Start()
        {
            _player = _unitsHolder.GetPlayer().publicTransform;
            var newPosition = _player.transform.position + _cameraOffset;
            _transform.position = newPosition;
        }

        private void LateUpdate()
        {
            var newPosition = _player.transform.position + _cameraOffset;
            _transform.position = Vector3.Slerp(_transform.position, newPosition, _smoothFactor);
        }
    }
}