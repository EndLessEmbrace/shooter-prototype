﻿using MyProject.Controllers;
using MyProject.Units;
using MyProject.Weapons;
using UnityEngine;

namespace MyProject.Players
{
    public class Player : Unit
    {
        [SerializeField] private PlayerMover _playerMover;
        [SerializeField] private PlayerInput _input;
        [SerializeField] private PlayerInputAttack _inputAttack;
        [SerializeField] private PlayerInventory _inventory;

        public PlayerInventory inventory => _inventory;
        public PlayerInput input => _input;
        public PlayerInputAttack inputAttack => _inputAttack;

        private void OnEnable()
        {
            HpChange += OnHPChange;
            Respawn += OnRespawn;
        }

        private void OnDisable()
        {
            HpChange -= OnHPChange;
            Respawn -= OnRespawn;
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                TakeDamage(1);
            }
        }

        private void OnHPChange(float amount)
        {
            if (amount <= 0)
            {
                ActivatePlayerInput(false);
            }
        }

        private void OnRespawn()
        {
            ActivatePlayerInput(true);

            foreach (var weapon in _inventory.weaponry.allWeapons)
            {
                weapon.Initialize();
            }
        }

        private void ActivatePlayerInput(bool value)
        {
            _inputAttack.enabled = value;
            _playerMover.enabled = value;
            _input.enabled = value;
        }
    }
}