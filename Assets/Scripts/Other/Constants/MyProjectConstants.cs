﻿namespace MyProject.Constants
{
    public static class MyProjectConstants
    {
        public const string pathRussianLocalization = "/LocalizationJson/RussianLocalization.json";
        public const string pathEnglishLocalization = "/LocalizationJson/EnglishLocalization.json";

        public const string pathUITimingsData = "/GameJson/UITimingsData.json";

        public const string pathGrenadeData = "/WeaponJson/Grenade/GrenadeData.json";
        public const string pathAutomaticData = "/WeaponJson/Automatic.json";
        public const string pathBotAutomaticData = "/WeaponJson/BotAutomatic.json";
        public const string pathShotgunData = "/WeaponJson/Shotgun.json";

        public const float botMinTargetDistance = 0.5f;
        public const float targetPositionY = 0.5f;
    }
}