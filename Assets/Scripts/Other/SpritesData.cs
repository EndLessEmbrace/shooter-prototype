﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace MyProject.Other
{
    public class SpritesData : MonoBehaviour
    {
        private static SpritesData _instance;
        public static SpritesData instance => _instance ?? (_instance = FindObjectOfType<SpritesData>());

        [SerializeField] private List<SpriteData> _spritesData = new List<SpriteData>();

        public Sprite GetSprite(string id)
        {
            foreach (var data in _spritesData)
            {
                if (data.id == id)
                    return data.sprite;
            }

            return null;
        }
    }

    [Serializable]
    public class SpriteData
    {
        [SerializeField] private string _id;
        [SerializeField] private Sprite _sprite;

        public string id => _id;
        public Sprite sprite => _sprite;
    }
}