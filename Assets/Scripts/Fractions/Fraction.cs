﻿using UnityEngine;

namespace MyProject.Fractions
{
    public class Fraction : MonoBehaviour
    {
        [SerializeField] private FractionType _type;

        public FractionType type => _type;
    }

    public enum FractionType
    {
        White = 0,
        Red = 10
    }
}
