﻿using System.Collections.Generic;
using MyProject.Units;
using UnityEngine;

namespace MyProject.Fractions
{
    public class FractionHolder : MonoBehaviour
    {
        [SerializeField] private UnitsHolder _unitsHolder;

        public List<Unit> GetAllUnitsByFraction(FractionType fractionType)
        {
            List<Unit> units = new List<Unit>();

            foreach (var unit in _unitsHolder.GetAllUnits())
            {
                if (unit.fraction.type.Equals(fractionType))
                {
                    units.Add(unit);
                }
            }

            return units;
        }

        public List<string> GetAllIdsByFraction(FractionType fractionType)
        {
            var units = GetAllUnitsByFraction(fractionType);
            List<string> unitsIds = new List<string>();

            foreach (var unit in units)
            {
                unitsIds.Add(unit.id);
            }

            return unitsIds;
        }
    }
}