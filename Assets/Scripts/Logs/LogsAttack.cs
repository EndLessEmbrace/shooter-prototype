﻿using MyProject.Players;
using MyProject.Units;
using MyProject.Weapons;
using UnityEngine;

namespace MyProject.Logs
{
    public class LogsAttack : MonoBehaviour
    {
        [SerializeField] private UnitsHolder _units;

        private Player _player;

        private void Start()
        {
            _player = _units.GetPlayer();

            foreach (var weapon in _player.inventory.weaponry.allWeapons)
            {
                weapon.Shoot += OnShoot;
            }
        }

        private void OnShoot(DataShoot data)
        {
            Debug.Log("________________________________");
            Debug.Log($"Стрелял: <color=white><b>{data.idAttacker.Substring(0, 6)}</b></color> из <color=white>{data.idWeapon}</color>. Урон: {data.damage}");
        }

        private void OnDestroy()
        {
            foreach (var weapon in _player.inventory.weaponry.allWeapons)
            {
                weapon.Shoot -= OnShoot;
            }
        }
    }
}