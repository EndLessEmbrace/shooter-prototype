﻿using MyProject.Fractions;
using UnityEngine;

namespace MyProject.Spawners
{
    public class SpawnPoint : MonoBehaviour
    {
        [SerializeField] private FractionType _fractionType;

        public FractionType fractionType => _fractionType;
    }
}