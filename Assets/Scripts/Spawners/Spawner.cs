﻿using System.Collections.Generic;
using System.Linq;
using MyProject.Bots;
using MyProject.Fractions;
using MyProject.Players;
using MyProject.Units;
using UnityEngine;

namespace MyProject.Spawners
{
    public class Spawner : MonoBehaviour
    {
        [SerializeField] private Camera _camera;
        [SerializeField] private Player _playerPrefab;
        [SerializeField] private Bot _alliedBotPrefab;
        [SerializeField] private Bot _enemyBotPrefab;
        [SerializeField] private int _countAlliedBots;
        [SerializeField] private int _countEnemyBots;
        [SerializeField] private UnitsHolder _unitsHolder;
        [SerializeField] private FractionHolder _fractionHolder;

        private List<SpawnPoint> _spawnPoints = new List<SpawnPoint>();

        private Player _player;

        private void Awake()
        {
            _spawnPoints = GetComponentsInChildren<SpawnPoint>(true).ToList();

            SpawnPlayer();

            SpawnBots(_enemyBotPrefab, FractionType.Red, _countEnemyBots);
            SpawnBots(_alliedBotPrefab, FractionType.White, _countAlliedBots);

            InitializeHealthBar();
            InitializeBots();
        }

        private void InitializeHealthBar()
        {
            var whiteFractionUnits = _fractionHolder.GetAllUnitsByFraction(FractionType.White);
            var redFractionUnits = _fractionHolder.GetAllUnitsByFraction(FractionType.Red);

            foreach (var unit in whiteFractionUnits)
            {
                unit.unitHealthBar.Initialize(unit);
            }

            foreach (var unit in redFractionUnits)
            {
                unit.unitHealthBar.Initialize(whiteFractionUnits[0]);
            }
        }

        private void InitializeBots()
        {
            foreach (var bot in _unitsHolder.GetAllBots())
            {
                bot.botMover.Initialize(_unitsHolder);
            }
        }

        private void SpawnPlayer()
        {
            var spawnPoints = _spawnPoints.Where(t => t.fractionType == FractionType.White).ToList();
            var spawnPoint = GetRandomPoint(spawnPoints);

            _player = Instantiate(_playerPrefab, spawnPoint.transform.position, spawnPoint.transform.rotation);
            _player.input.Initialize(_camera);
            _player.inputAttack.Initialize(_player);
            _player.unitSpawn.SetSpawnPoint(spawnPoint);
            _player.Initialize(_unitsHolder);
            _unitsHolder.Add(_player);
        }

        private void SpawnBots(Bot prefab, FractionType type, int countBots)
        {
            var spawnPointsBots = _spawnPoints.Where(t => t.fractionType == type && t != _player.unitSpawn.spawnPoint).ToList();

            for (int i = 0; i < countBots; i++)
            {
                if (spawnPointsBots?.Count <= 0)
                    return;

                var spawnPoint = GetRandomPoint(spawnPointsBots);
                spawnPointsBots.Remove(spawnPoint);

                var bot = Instantiate(prefab, spawnPoint.transform.position, spawnPoint.transform.rotation);
                bot.unitSpawn.SetSpawnPoint(spawnPoint);
                bot.Initialize(_unitsHolder);
                _unitsHolder.Add(bot);
            }
        }

        private SpawnPoint GetRandomPoint(List<SpawnPoint> spawnPoints)
        {
            return spawnPoints[Random.Range(0, spawnPoints.Count)];
        }
    }
}