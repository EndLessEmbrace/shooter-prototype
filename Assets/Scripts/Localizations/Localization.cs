﻿using System;
using System.Collections.Generic;
using System.IO;
using MyProject.Constants;
using Newtonsoft.Json;
using UnityEngine;

namespace MyProject.Localizations
{
    public static class Localization
    {
        public static event Action ChangeLanguage = delegate { };

        private static Dictionary<string, string> russianLocalization = new Dictionary<string, string>();
        private static Dictionary<string, string> englishLocalization = new Dictionary<string, string>();

        private static Dictionary<string, string> currentLocalization = new Dictionary<string, string>();

        public static void Initialize()
        {
            var russianLocalizationFile = File.ReadAllText(Application.dataPath + MyProjectConstants.pathRussianLocalization);
            var englishLocalizationFile = File.ReadAllText(Application.dataPath + MyProjectConstants.pathEnglishLocalization);

            russianLocalization = JsonConvert.DeserializeObject<Dictionary<string, string>>(russianLocalizationFile);
            englishLocalization = JsonConvert.DeserializeObject<Dictionary<string, string>>(englishLocalizationFile);

            SetLanguage(Language.English);
        }

        public static string GetLocalizationString(string key)
        {
            return currentLocalization[key];
        }

        public static void SetLanguage(Language language)
        {
            switch (language)
            {
                case Language.Russian:
                    {
                        currentLocalization = russianLocalization;
                        break;
                    }
                case Language.English:
                    {
                        currentLocalization = englishLocalization;
                        break;
                    }
                default:
                    {
                        throw new Exception("There is no such language");
                    }
            }
            ChangeLanguage?.Invoke();
        }
    }

    public enum Language
    {
        Russian = 0,
        English = 1
    }
}