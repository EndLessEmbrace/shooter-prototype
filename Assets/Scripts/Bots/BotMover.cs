﻿using System.Collections.Generic;
using MyProject.Constants;
using MyProject.Units;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

namespace MyProject.Bots
{
    [RequireComponent(typeof(Bot))]
    [RequireComponent(typeof(NavMeshAgent))]
    public class BotMover : MonoBehaviour
    {
        [Header("Values targetPosition")]
        [SerializeField] private float _minTargetPosition = -20;
        [SerializeField] private float _maxTargetPosition = 20f;

        [Header("Parameters")]
        [SerializeField] private float _sightDistance = 5f;

        private Bot _bot;
        private NavMeshAgent _agent;
        private Vector3 _targetPosition = Vector3.zero;
        private Transform _transform;

        public List<Unit> enemyUnits { get; private set; } = new List<Unit>();
        public Unit targetUnit { get; private set; }

        private void Awake()
        {
            _transform = transform;
            _agent = GetComponent<NavMeshAgent>();
            _bot = GetComponent<Bot>();

            _agent.autoBraking = false;
        }

        private void Start()
        {
            RandomTarget();

            targetUnit.Respawn += Activate;
            targetUnit.Dead += RandomTarget;
        }

        private void OnDestroy()
        {
            targetUnit.Respawn -= Activate;
            targetUnit.Dead -= RandomTarget;
        }

        private void OnCollisionEnter(Collision collision)
        {
            PickRandomPosition();
        }

        private void FixedUpdate()
        {
            var playerDistance = (_transform.position - targetUnit.publicTransform.position).sqrMagnitude;

            if (_sightDistance * _sightDistance >= playerDistance)
            {
                _targetPosition = targetUnit.publicTransform.position;
            }
            else if (!_agent.pathPending && _agent.remainingDistance < MyProjectConstants.botMinTargetDistance)
            {
                PickRandomPosition();
            }
        }

        public void Initialize(UnitsHolder unitsHolder)
        {
            foreach (var unit in unitsHolder.GetAllUnits())
            {
                if (!unit.fraction.type.Equals(_bot.fraction.type))
                {
                    enemyUnits.Add(unit);
                }
            }
        }

        public void Activate()
        {
            _agent.isStopped = false;
            enabled = true;
        }

        public void Deactivate()
        {
            _agent.isStopped = true;
            enabled = false;
        }

        private void RandomTarget()
        {
            targetUnit = enemyUnits[Random.Range(0, enemyUnits.Count)];
            if (targetUnit != null)
            {
                Activate();
                PickRandomPosition();
            }
        }

        private void PickRandomPosition()
        {
            _targetPosition.x = Random.Range(_minTargetPosition, _maxTargetPosition);
            _targetPosition.z = Random.Range(_minTargetPosition, _maxTargetPosition);
            _targetPosition.y = MyProjectConstants.targetPositionY;

            if (_agent.isActiveAndEnabled)
            {
                _agent.destination = _targetPosition;
            }
        }
    }
}