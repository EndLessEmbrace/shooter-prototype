﻿using MyProject.Units;
using UnityEngine;

namespace MyProject.Bots
{
    public class Bot : Unit
    {
        [SerializeField] private BotMover _botMover;
        [SerializeField] private BotAttack _botAttack;
        public BotMover botMover => _botMover;

        private void OnEnable()
        {
            HpChange += OnHpChange;
            Respawn += OnRespawn;
        }

        private void OnDisable()
        {
            HpChange -= OnHpChange;
            Respawn -= OnRespawn;
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                TakeDamage(1);
            }
        }

        private void OnHpChange(float amount)
        {
            if (amount <= 0)
            {
                Deactivate();
            }
        }

        private void OnRespawn()
        {
            Activate();
        }

        private void Activate()
        {
            _botAttack.Activate();
            _botMover.Activate();
        }

        private void Deactivate()
        {
            _botAttack.Deactivate();
            _botMover.Deactivate();
        }
    }
}