﻿using System.Collections.Generic;
using UnityEngine;

namespace MyProject.Bots
{
    public class BotRandomMesh : MonoBehaviour
    {
        [SerializeField] private MeshRenderer _meshRenderer;
        [SerializeField] private List<Material> _materials = new List<Material>();

        private void Awake()
        {
            _meshRenderer.sharedMaterial = _materials[Random.Range(0, _materials.Count)];
        }
    }
}

