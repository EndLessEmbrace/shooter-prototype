﻿using System.Collections;
using MyProject.Weapons;
using UnityEngine;

namespace MyProject.Bots
{
    [RequireComponent(typeof(Bot))]
    [RequireComponent(typeof(BotMover))]
    public class BotAttack : MonoBehaviour
    {
        [SerializeField] private Weapon _weapon;
        [SerializeField] private float _rotationSpeed;

        private Bot _bot;
        private BotMover _mover;
        private bool _attacking = false;
        private Coroutine _attackCoroutine = null;
        private Coroutine _rotateCoroutine = null;

        private Transform _transform;

        private void Start()
        {
            _transform = transform;

            _mover = GetComponent<BotMover>();
            _bot = GetComponent<Bot>();

            _mover.targetUnit.Respawn += Activate;
            _mover.targetUnit.Dead += Deactivate;

            _weapon.Initialize(_bot);
        }

        private void OnDestroy()
        {
            _mover.targetUnit.Respawn -= Activate;
            _mover.targetUnit.Dead -= Deactivate;
        }

        private void Update()
        {
            if (_attacking)
                return;

            var playerDistance = (_transform.position - _mover.targetUnit.publicTransform.position).sqrMagnitude;
            if (playerDistance <= _weapon.attackDistance * _weapon.attackDistance)
            {
                _attacking = true;
                _attackCoroutine = StartCoroutine(Attack());
                _rotateCoroutine = StartCoroutine(Rotate());
            }
        }

        public void Activate()
        {
            enabled = true;
        }

        public void Deactivate()
        {
            if (_attackCoroutine != null)
            {
                StopCoroutine(_attackCoroutine);
            }

            if (_rotateCoroutine != null)
            {
                StopCoroutine(_rotateCoroutine);
            }

            _attacking = false;
            enabled = false;
        }

        private IEnumerator Rotate()
        {
            while (true)
            {
                var targetRotation = Quaternion.LookRotation(_mover.targetUnit.publicTransform.position - _bot.publicTransform.position);
                _bot.publicTransform.rotation = Quaternion.Slerp(_bot.publicTransform.rotation, targetRotation, Time.deltaTime * _rotationSpeed);

                yield return null;
            }
        }

        private IEnumerator Attack()
        {
            _mover.Deactivate();

            yield return new WaitForSeconds(_weapon.weaponData.rateOfFire);

            var playerDistance = (_transform.position - _mover.targetUnit.publicTransform.position).sqrMagnitude;
            if (playerDistance <= _weapon.attackDistance * _weapon.attackDistance)
            {
                _weapon.Fire();
            }
            else
            {
                _mover.Activate();
            }

            _attacking = false;
            _attackCoroutine = null;

            if (_rotateCoroutine != null)
            {
                StopCoroutine(_rotateCoroutine);
            }
        }
    }
}