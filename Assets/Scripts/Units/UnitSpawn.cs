﻿using MyProject.Spawners;
using UnityEngine;

namespace MyProject.Units
{
    public class UnitSpawn : MonoBehaviour
    {
        public SpawnPoint spawnPoint { get; private set; } = null;

        public void SetSpawnPoint(SpawnPoint spawnPoint)
        {
            if (this.spawnPoint == null)
            {
                this.spawnPoint = spawnPoint;
            }
        }

        public void Respawn()
        {
            transform.position = spawnPoint.transform.position;
        }
    }
}
