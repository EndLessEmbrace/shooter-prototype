﻿using System;
using System.Collections;
using MyProject.Fractions;
using MyProject.UI.Data;
using MyProject.UI.Units;
using MyProject.Weapons.Data;
using Newtonsoft.Json;
using UnityEngine;

namespace MyProject.Units
{
    public abstract class Unit : MonoBehaviour
    {
        public event Action<float> HpChange = delegate { };
        public event Action Dead = delegate { };
        public event Action Respawn = delegate { };

        [SerializeField] private UnitHealthBar _unitHealthBar;
        [SerializeField] private UnitSpawn _unitSpawn;
        [SerializeField] private Fraction _fraction;

        [Header("Parameters")]
        [SerializeField] private float _moveSpeed = 3.5f;
        [SerializeField] private float _currentHealth = 3;
        [SerializeField] private float _maxHealth = 3;

        [Header("Timings")]
        [SerializeField] private TextAsset _uiTimingsData;

        private UITimingsData _data;

        public Transform publicTransform { get; private set; }
        public string id { get; private set; } = Guid.NewGuid().ToString();
        public Vector3 aimDistanceHelper { get; private set; }
        public UnitsHolder unitsHolder { get; private set; }

        public UnitHealthBar unitHealthBar => _unitHealthBar;
        public UnitSpawn unitSpawn => _unitSpawn;
        public Fraction fraction => _fraction;

        public float moveSpeed => _moveSpeed;
        public float currentHealth => _currentHealth;
        public float maxHealth => _maxHealth;

        private void Awake()
        {
            publicTransform = transform;
            var uiTimingsData = _uiTimingsData.text;
            _data = JsonConvert.DeserializeObject<UITimingsData>(uiTimingsData);
        }

        public void SetAimDistance(Vector3 aimDistance)
        {
            aimDistanceHelper = aimDistance;
        }

        public void Initialize(UnitsHolder unitsHolder)
        {
            this.unitsHolder = unitsHolder;
        }

        public void TakeDamage(int amount)
        {
            if (_currentHealth <= 0)
                return;

            _currentHealth -= amount;

            if (_currentHealth <= 0)
            {
                Dead?.Invoke();
                unitsHolder.Remove(this);
                StartCoroutine(Death());
            }

            HpChange?.Invoke(_currentHealth);
        }

        private IEnumerator Death()
        {
            yield return new WaitForSeconds(_data.respawnTime);
            _currentHealth = _maxHealth;
            unitSpawn.Respawn();
            Respawn?.Invoke();
            unitsHolder.Add(this);
        }
    }
}