﻿using System.Collections.Generic;
using MyProject.Bots;
using MyProject.Players;
using UnityEngine;

namespace MyProject.Units
{
    public class UnitsHolder : MonoBehaviour
    {
        private List<Unit> _units = new List<Unit>();

        public Player GetPlayer()
        {
            foreach (var unit in _units)
            {
                if (unit is Player player)
                    return player;
            }

            return null;
        }

        public List<Unit> GetAllUnits()
        {
            List<Unit> allUnits = new List<Unit>();

            foreach (var unit in _units)
            {
                if (unit != null)
                    allUnits.Add(unit);
            }

            return allUnits;
        }

        public List<Bot> GetAllBots()
        {
            List<Bot> bots = new List<Bot>();

            foreach (var unit in _units)
            {
                if (unit is Bot bot)
                {
                    bots.Add(bot);
                }
            }

            return bots;
        }

        public void Add(Unit unit)
        {
            if (unit != null)
            {
                _units.Add(unit);
                Debug.Log($"UnitsHolder add unit with ID: {unit.id}.\t Count units:" + _units.Count);
            }
        }

        public void Remove(Unit unit)
        {
            if (unit != null)
            {
                _units.Remove(unit);
                Debug.Log($"UnitsHolder remove unit with ID: {unit.id}.\t Count units:" + _units.Count);
            }
        }
    }
}