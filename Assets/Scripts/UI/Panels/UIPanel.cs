﻿using UnityEngine;

namespace MyProject.UI.Panels
{
    public class UIPanel : MonoBehaviour
    {
        public void Show()
        {
            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}
