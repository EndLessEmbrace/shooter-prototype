﻿using MyProject.Players;
using MyProject.Units;
using MyProject.Weapons;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace MyProject.UI.Panels
{
    public class UIWeaponPanel : UIPanel
    {
        [SerializeField] private UnitsHolder _unitsHolder;

        [SerializeField] private Image _reloadImage;
        [SerializeField] private Image _icon;
        [SerializeField] private TMP_Text _currentCountAmmunitionText;
        [SerializeField] private TMP_Text _maxCountAmmunitionText;

        private Player _player;

        private void Awake()
        {
            _player = _unitsHolder.GetPlayer();
            _player.Respawn += WeaponChange;
            _player.inventory.ChangeWeapon += WeaponChange;
        }

        private void Start()
        {
            _player.inventory.currentWeapon.Shoot += OnShoot;
            _player.inventory.currentWeapon.StartReload += OnStartReload;
            _player.inventory.currentWeapon.EndingReload += OnEndingReload;
        }

        private void OnDestroy()
        {
            _player.inventory.currentWeapon.Shoot -= OnShoot;
            _player.inventory.currentWeapon.StartReload -= OnStartReload;
            _player.inventory.currentWeapon.EndingReload -= OnEndingReload;

            _player.Respawn -= WeaponChange;
            _player.inventory.ChangeWeapon -= WeaponChange;
        }

        private void WeaponChange()
        {
            var currentWeaponPrev = _player.inventory.currentWeapon;

            if (currentWeaponPrev != null)
            {
                _player.inventory.currentWeapon.Shoot -= OnShoot;
                _player.inventory.currentWeapon.StartReload -= OnStartReload;
                _player.inventory.currentWeapon.EndingReload -= OnEndingReload;
            }

            var currentWeapon = _player.inventory.currentWeapon;

            _player.inventory.currentWeapon.Shoot += OnShoot;
            _player.inventory.currentWeapon.StartReload += OnStartReload;
            _player.inventory.currentWeapon.EndingReload += OnEndingReload;

            _icon.sprite = currentWeapon.weaponData.icon;
            _currentCountAmmunitionText.text = currentWeapon.currentCountAmmunition.ToString();
            _maxCountAmmunitionText.text = currentWeapon.maxCountAmmunition.ToString();
        }

        private void OnShoot(DataShoot data)
        {
            ShowCurrentCountAmmunition();
        }

        private void OnStartReload()
        {
            _reloadImage.gameObject.SetActive(true);
        }

        private void OnEndingReload()
        {
            _reloadImage.gameObject.SetActive(false);
            ShowCurrentCountAmmunition();
        }

        private void ShowCurrentCountAmmunition()
        {
            var currentWeapon = _player.inventory.currentWeapon;
            _currentCountAmmunitionText.text = currentWeapon.currentCountAmmunition.ToString();
        }
    }
}
