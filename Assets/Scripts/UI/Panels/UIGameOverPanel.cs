﻿using System.Collections;
using MyProject.UI.Data;
using MyProject.Units;
using Newtonsoft.Json;
using TMPro;
using UnityEngine;

namespace MyProject.UI.Panels
{
    public class UIGameOverPanel : UIPanel
    {
        [SerializeField] private UIWeaponPanel _weaponPanel;
        [SerializeField] private TextAsset _data;
        [SerializeField] private UnitsHolder _unitsHolder;
        [SerializeField] private TMP_Text _timerText;

        private Unit _player;
        private UITimingsData _uiTimingsData;

        private void Start()
        {
            _player = _unitsHolder.GetPlayer();
            _player.Dead += OnDead;
            _player.Respawn += OnRespawn;

            var timingsData = _data.text;
            _uiTimingsData = JsonConvert.DeserializeObject<UITimingsData>(timingsData);

            Hide();
        }

        private void OnDestroy()
        {
            _player.Dead -= OnDead;
            _player.Respawn -= OnRespawn;
        }

        private void OnRespawn()
        {
            Hide();
            _weaponPanel.Show();
        }

        private void OnDead()
        {
            _weaponPanel.Hide();

            Show();

            StartCoroutine(Timer());
        }

        private IEnumerator Timer()
        {
            float timeValue = _uiTimingsData.respawnTime;

            while (true)
            {
                timeValue--;
                _timerText.text = timeValue.ToString("0");

                yield return new WaitForSeconds(1);

                if (timeValue < 0)
                    yield break;
            }
        }
    }
}
