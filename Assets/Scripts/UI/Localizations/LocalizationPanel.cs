﻿using MyProject.Localizations;
using UnityEngine;
using UnityEngine.UI;

namespace MyProject.UI.Localizations
{
    public class LocalizationPanel : MonoBehaviour
    {
        [SerializeField] private Button _russianLocalizationButton;
        [SerializeField] private Button _englishLocalizationButton;

        private void Awake()
        {
            Localization.Initialize();
        }

        private void OnEnable()
        {
            _russianLocalizationButton.onClick.AddListener(() => SetLanguage(Language.Russian));
            _englishLocalizationButton.onClick.AddListener(() => SetLanguage(Language.English));
        }

        private void OnDisable()
        {
            _russianLocalizationButton.onClick.RemoveListener(() => SetLanguage(Language.Russian));
            _englishLocalizationButton.onClick.RemoveListener(() => SetLanguage(Language.English));
        }

        private void SetLanguage(Language language)
        {
            Localization.SetLanguage(language);
        }
    }
}
