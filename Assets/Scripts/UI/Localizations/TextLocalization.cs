﻿using MyProject.Localizations;
using TMPro;
using UnityEngine;

namespace MyProject.UI.Localizations
{
    [RequireComponent(typeof(TMP_Text))]
    public class TextLocalization : MonoBehaviour
    {
        [SerializeField] private string _id;

        private TMP_Text _text;

        private void OnEnable()
        {
            _text = GetComponent<TMP_Text>();

            OnChangeLanguage();
            Localization.ChangeLanguage += OnChangeLanguage;
        }

        private void OnDisable()
        {
            Localization.ChangeLanguage -= OnChangeLanguage;
        }

        private void OnChangeLanguage()
        {
            _text.text = Localization.GetLocalizationString(_id);
        }
    }
}