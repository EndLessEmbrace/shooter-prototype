﻿using MyProject.Players;
using MyProject.Units;
using TMPro;
using UnityEngine;

namespace MyProject.UI.Units
{
    public class UnitHealthBar : MonoBehaviour
    {
        [SerializeField] private Unit _unit;
        [SerializeField] private GameObject _scullImg;
        [SerializeField] private GameObject _bar;
        [SerializeField] private SpriteRenderer _barImg;
        [SerializeField] private TMP_Text _text;

        [Header("Colors")]
        [SerializeField] private Color _redColor;
        [SerializeField] private Color _greenColor;

        private void Awake()
        {
            Reset();
            _unit.HpChange += OnHpChange;
            _unit.Respawn += OnRespawn;
        }

        private void LateUpdate()
        {
            _scullImg.transform.rotation = Camera.main.transform.rotation;
            _bar.transform.rotation = Camera.main.transform.rotation;
        }

        public void Initialize(Unit unit)
        {
            _barImg.color = unit.fraction.type.Equals(_unit.fraction.type) ? _greenColor : _redColor;
        }

        private void OnRespawn()
        {
            Reset();
        }

        private void OnHpChange(float health)
        {
            var frac = health / _unit.maxHealth;
            _text.text = $"{_unit.currentHealth:####}/{_unit.maxHealth:####}";
            _barImg.size = new Vector2(frac, _barImg.size.y);
            var pos = _barImg.transform.localPosition;
            pos.x = -(1 - frac) / 2;
            _barImg.transform.localPosition = pos;
            _bar.SetActive(!(health <= 0));
            _scullImg.SetActive((health <= 0));
        }

        private void Reset()
        {
            _text.text = $"{_unit.currentHealth:####}/{_unit.maxHealth:####}";
            _bar.SetActive(true);
            _scullImg.SetActive(false);
            OnHpChange(_unit.currentHealth);
        }
    }
}